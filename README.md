GIthub使用指北:

**1.想将项目拷贝到自己帐号下就fork一下.**

**2.持续关注项目更新就star一下**

**3.watch是设置接收邮件提醒的.**

# Darknet19-pytorch

采用了与torchvison中vgg相似的实现方法.

( The implementation method is similar to that of VGG in torchvision. )

在使用时,与调用torchvision.models中模型相似.

(you can use it same as models of the torchvision)

welcome **star**, **fork**, **watch**.

---

The model structure:
![image](imgs/model.png)

